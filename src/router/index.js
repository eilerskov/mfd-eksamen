import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import BooksView from "../views/BooksView.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "about",
    component: function () {
      return import("../views/AboutView.vue");
    },
  },
  {
    path: "/contact",
    name: "contact",
    component: function () {
      return import("../views/ContactView.vue");
    },
  },
  {
    path: "/books/:id",
    name: "books",
    component: BooksView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
