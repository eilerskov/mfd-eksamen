import Vue, { createApp } from "vue";

import App from "./App.vue";
import router from "./router";
import { createPinia } from "pinia";
import mitt from "mitt";

const emitter = mitt();

const app = createApp(App);

app.config.globalProperties.emitter = emitter;

app.use(createPinia());
app.use(router).mount("#app");
